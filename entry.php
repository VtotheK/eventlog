<?php
class entry{
	public $ID;
	public $name;
	public $message;
	public $time;
	public $date;
	public $replyTo;
	public $area;
	public $replies = array();
	
	public function __construct($ID,$name,$date,$time,$replyTo,$area,$message)
	{
		$this->date = $date;
		$this->ID = $ID;
		$this->name = $name;
		$this->message = $message;
		$this->time = $time;
		$this->replyTo = $replyTo;
		$this->area = $area;
	}
}
?>