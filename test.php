	<?php $errorfield = $tempID = $tempMessage = $formVisibility = ""?>
	<?php require 'entry.php'?>
	<?php require 'getposts.php'?>
	<?php require 'createentry.php'?>

	<!DOCTYPE html>
	<html>
	<head>
		<?php echo "<script type='text/javascript' src='test.js'></script>"?>
		<?php echo '<link rel="stylesheet" type="text/css" href="test.css"/>'?>
		<title></title>
	</head>
	<body>
		<?php addEntry();?>

		    <button id="addentrybtn" onclick="showEntryForm()">LISÄÄ TAPAHTUMA</button>
	        
	        <div style="<?php echo $formVisibility?>" id="textfield">
	            <form id="messagefield" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
	            <input type = "number" id = "henkilonumero" name="userID" placeholder="Henkilönumero" value="<?php echo $tempID?>"/><span id="nousererror"><?php echo $errorfield?></span><br>
	            <textarea type = "text" name="message" id = "viesti" placeholder="Kirjoita tapahtuma"><?php echo $tempMessage?></textarea><br>
	                <div id="aluelaatikko">
	                    <p id="aluetext">ALUE</p>
	                    <input name="alue" value="1" class = "alueet" type = "radio"><label class="aluelabel">ML1</label>
	                    <input name="alue" value="2" class = "alueet" type = "radio"><label class="aluelabel">ML2</label>
	                    <input name="alue" value="3" class = "alueet" type = "radio"><label class="aluelabel">ML3</label>
	                    <input name="alue" value="4" class = "alueet" type = "radio"><label class="aluelabel">ML4</label>
	                    <input name="alue" value="5" class = "alueet" type = "radio"><label class="aluelabel">ML5</label>
	                    <input name="alue" value="6" class = "alueet" type = "radio"><label class="aluelabel">ML6</label>
	                    <input name="alue" value="7" class = "alueet" type = "radio"><label class="aluelabel">ML7</label><br>
	                    <input name="alue" value="8" class = "alueet" type = "radio"><label class="aluelabel">VHJ</label>
	                    <input name="alue" value="Mampa" class = "alueet" type = "radio"><label class="aluelabel">Mampa</label>
	                    <input name="alue" value="Multipick" class = "alueet" type = "radio"><label class="aluelabel">Multipick</label>
	                    <input name="alue" value="Sekvensseri" class = "alueet" type = "radio"><label class="aluelabel">Sekvensseri</label>
	                    <input name="alue" value="Reject" class = "alueet" type = "radio"><label class="aluelabel">Reject</label>
	                    <input name="alue" value="Lavaus" class = "alueet" type = "radio"><label class="aluelabel">Lavaus</label>
	                    <input name="alue" value="Infeed" class = "alueet" type = "radio"><label class="aluelabel">Infeed</label><br>
	                    <input id="laheta" type="submit"/>
	                    </form>
	                </div>
	        </div>
	    	<?php getPosts();?>
	</body>
	</html>
	<?php
		function addEntry()
		{
			require 'dbh.inc.php';
			if($_SERVER["REQUEST_METHOD"] == "POST") //TODO HOW TO GET REPLYTO AND AREA VALUES FROM POST
			{
				$cachedMessage = $cachedID = null;
				
				if(isset($_POST['userID']) && !empty($_POST['userID']))
				{
					$cachedID = $_POST['userID'];
				}
				if(isset($_POST['message']) && !empty($_POST['message']))
				{ 
					$cachedMessage = $_POST['message'];
				}
				global $tempMessage, $tempID,$errorfield,$formVisibility;
				$formVisibility = "display : block;";
				$tempMessage = $cachedMessage;
				$tempID = $cachedID;
				if(empty($cachedID) and empty($cachedMessage))
				{
					$errorfield = "Tyhjä käyttäjätunnus ja viesti!";
					return;
				}
				else if(empty($cachedID))
				{
					$errorfield = "Tyhjä käyttäjätunnus!";
					return;
				}
				else if(empty($cachedMessage))
				{
					$errorfield = "Tyhjä viesti!";
					return;
				}
				else if(!isset($_POST['alue']))
				{
					$errorfield = "Aluetta ei valittu!";
					return;
				}
				$area = $_POST['alue'];

				if($conn->connect_errno)
				{
					die("Could not connect to MySQL at addEntry()" . $conn->connect_error);
					exit();
				}

				$id = $conn->real_escape_string($_POST['userID']);
				$entry = $conn->real_escape_string($_POST['message']);
				if($validate = $conn->prepare("SELECT Name FROM users WHERE ID=?"))
				{
					$validate->bind_param('d',$id);
					if(!$validate->execute())
					{
						echo("ID Validation execution failed (" .$conn->errno.")".$conn->error);
					}
					$res = $validate->get_result();
					if($res->num_rows < 1)
					{
						$errorfield = "Käyttäjää ei löytynyt!";
						$formVisibility = "display:block";
					}

					else
					{
						$formVisibility = "display:none";
						$tempMessage = $tempID = "";
						$user = $res->fetch_assoc()["Name"];
						createEntry($user,$entry,$area);
					}
						
				}
				else
				{
					die("Invalid query at line 94");
				}
			}
		}
	?>