	<?php

	function getPosts()
	{
		require 'dbh.inc.php';
		require 'cmp.inc.php';
		$q = "SELECT ID,User,Message,ReplyTo, DATE_FORMAT(SendDate, '%d.%c.%Y') SendDate, TIME_FORMAT(SendDate, '%H:%i:%s') SendTime, Area 
		FROM posts ORDER BY SendDate ASC";
		$rows = [];
		$mainent = [];
		$replies = [];
		if($conn-> connect_errno)
		{
			echo "Failed to connect MySQL";
			exit();
		}
		if($result = $conn -> query($q))
		{
			if(mysqli_num_rows($result) == 0)
			{
				echo "false";
				exit();
			}
			$m=0;
			$r=0;
			while($row = mysqli_fetch_assoc($result))
			{
				if($row['ReplyTo'] == null)
				{
					$mainent[$m] = new entry($row['ID'],
						$row['User'],
						$row['SendDate'],
						$row['SendTime'],
						$row['ReplyTo'],
						$row['Area'],
						$row['Message']);
					$m++;
				}
				else
				{
					$replies[$r] = new entry($row['ID'],
						$row['User'],
						$row['SendDate'],
						$row['SendTime'],
						$row['ReplyTo'],
						$row['Area'],
						$row['Message']);
					$r++;
				}
			}
		}
		$tempDate = null;
		$r=0;

		for($i = 0; $i <count($mainent); $i++)
		{
			for($j = 0; $j<count($replies); $j++)
			{
				if($mainent[$i]->ID == $replies[$j]->replyTo)
				{
					array_push($mainent[$i]->replies, clone $replies[$j]);
				}
			}
		}

		arsort($mainent);
		foreach ($mainent as $main) 
		{
			if(count($main->replies)>0)
			{
				foreach ($main->replies as $rep) {
					uasort($main->replies, "cmp");
				}
			}
		}

			foreach($mainent as $ent)
			{
				if($tempDate != $ent->date)
				{
					echo'<h1>'.$ent->date.'</h1>';
				}
				echo '<div value="'.$ent->ID.'" class="entrydiv">
				<header class="entryheader">POSTID:'.$ent->ID.'Alue:'.$ent->area.'&nbsp&nbsp&nbsp&nbsp&nbsp Aika: '.$ent->date.'&nbsp'.$ent->time. '&nbsp&nbsp&nbsp&nbsp&nbsp Merkinnän lisääjä:'.$ent->name.
				'&nbsp&nbsp&nbsp&nbsp&nbsp <button class="replybtn">VASTAA</button></header>
				<pre class="entrybody">'.$ent->message.'</pre>';
				$tempDate = $ent->date;
				getallreplies($ent);
				echo '</div>';
			}
		}

		function getallreplies($ent)
		{
			if(count($ent->replies)>0)
			{
				foreach ($ent->replies as $rep) {
					echo '<div value="'.$rep->ID.'" class="replydiv">
					<header>POSTID:'.$rep->ID.'Alue:'.$rep->area.'&nbsp&nbsp&nbsp&nbsp&nbsp Aika:'.$rep->date.'&nbsp'.$rep->time. '&nbsp&nbsp&nbsp&nbsp&nbsp Kommentin lisääjä:'.$rep->name.
					'&nbsp&nbsp&nbsp&nbsp&nbsp</header>
					<pre class="entrybody">'.$rep->message.'</pre>
					</div>';
				}
			}
		}
		?>